// on défini les constantes
const container = document.querySelector(".container");
const inputSearch = document.querySelector("#choice-pays");
const datalist = document.querySelector("datalist");
const searchButton = document.querySelector("button[type=submit]");

/**
 * Fonction pour créer la liste datalist
 * pour l'autocomplétion
 * Et la mise en place du filtre sur le nom des pays
 */
const createList = () => {
    fetch(`https://restcountries.eu/rest/v2/all`)
        .then((res) => res.json())
        .then((data) => {
            // on crée un objet listant le nom des pays
            const allPays = data.map((paysOption) => {
                return {
                    nom: paysOption.name,
                };
            });

            // on crée les options de la datalist à partir de l'objet allPays
            allPays.forEach((onePays) => {
                const option = document.createElement("option");
                option.setAttribute("value", onePays.nom); // affiche le nom du pays dans <option>
                datalist.appendChild(option);
            });
        });
};

/**
 * Permet d'afficher la liste des pays en fonction
 * de l'argument passé en paramètre
 * Par défaut : affiche tous les pays
 * @param {string} search 
 */
const display = (search = "all") => {
    fetch(`https://restcountries.eu/rest/v2/${search}`)
        .then((res) => res.json())
        .then((data) => {
            // on crée un objet listant : nom en fr, capitale, population et drapeau
            const listePays = data.map((pays) => {
                return {
                    nomFr: pays.translations.fr,
                    capitale: pays.capital,
                    nbHabitants: pays.population,
                    drapeau: pays.flag,
                };
            });

            // on parcourt l'objet listePays pour créer une div contenant le drapeau et les infos du pays
            listePays.forEach((itemPays) => {

                // on crée la div pays pour accueillir les infos d'un pays
                const pays = document.createElement("div");
                pays.setAttribute("class", "pays");

                // on crée img pour mettre le drapeau
                const imgFlag = document.createElement("img");
                imgFlag.setAttribute("class", "flag");
                imgFlag.setAttribute("src", `${itemPays.drapeau}`);
                imgFlag.setAttribute("alt", `drapeau ${itemPays.nomFr}`);
                pays.appendChild(imgFlag);

                // on crée une ul pour faire la liste des infos
                const ul = document.createElement("ul");
                ul.setAttribute("class", "listePays");
                ul.innerHTML = `
                    <li class="infoPays"><strong>Nom : </strong>${itemPays.nomFr}</li>
                    <li class="infoPays"><strong>Capitale : </strong>${itemPays.capitale}</li>
                    <li class="infoPays"><strong>Nombre d'habitants : </strong>${(itemPays.nbHabitants).toLocaleString()} habitants</li>
                `;
                pays.appendChild(ul);
                container.appendChild(pays);

                // on adapte la hauteur de ul en fonction de la hauteur de la div
                // pour centrer verticalement via flexbox
                const heightPays = pays.offsetHeight;
                ul.style.height = `${heightPays}px`;
            });
        })
        .catch((error) => console.error(error));
};
createList(); // on appel de la fonction qui crée la liste d'autocomplétion
display(); // on appel la fonction qui affiche la liste des pays

searchButton.addEventListener("click", (e) => {
    e.preventDefault(); // pour éviter de recharger la page quand je soumets le formulaire
    container.innerHTML = ""; // on efface la liste actuelle pour préparer l'affichage de la recherche
    const valueSearch = "name/" + inputSearch.value; // on récupère la valeur du champs et j'ajoute "name/" pour correspondre à l'url de l'appel de l'API

    // condition si mon champs est "vide"
    if (valueSearch === null || valueSearch == "name/") {
        display(); // on affiche tout si c'est vide
    } else {
        display(valueSearch); // sinon on fait la recherche
    }
});