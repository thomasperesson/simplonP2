<?php
if (isset($_GET['value']) && $_GET['value'] !== null) {
    $error = "";
    $value = $_GET['value'];
} else {
    $error = "Veuillez entrez une valeur";
}

function convert($value)
{
    return round($value * 1.60934, 2, PHP_ROUND_HALF_UP);
}
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Convertisseur km vers miles</title>
</head>

<body>
    <div class="container">
        <h1>Convertisseur miles &rarr; km</h1>
        <form action="./index.php" method="GET">
            <label for="value">Entrez la valeur en miles :</label>
            <input type="number" name="value" id="value" min="0">
            <button type="submit">Convertir</button>
        </form>
        <?php
            if (isset($value) && $value !== null && is_numeric($value)) {
                 echo "<p class=\"value\">" . $value . " km &rarr; <span>" . convert($value) . " miles</span></p>";
            } else {
                echo "<p class=\"error\">" . $error . "</p>";
            }
           
        
            
        
        ?>
    </div>
</body>

</html>