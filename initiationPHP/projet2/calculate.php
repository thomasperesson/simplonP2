<?php
$num1 = $_GET['num1'] ?? 0;
$num2 = $_GET['num2'] ?? 0;
$operator = $_GET['operator'] ?? "+";

function calculate($num1, $num2, $operator)
{
    if ($operator !== "" && $operator === "+") {
        if (($num1 !== "" && is_numeric($num1)) && ($num2 !== "" && is_numeric($num2))) {
            return $num1 + $num2;
        }
    } elseif ($operator !== "" && $operator === "-") {
        if (($num1 !== "" && is_numeric($num1)) && ($num2 !== "" && is_numeric($num2))) {
            return $num1 - $num2;
        }
    } elseif ($operator !== "" && $operator === "*") {
        if (($num1 !== "" && is_numeric($num1)) && ($num2 !== "" && is_numeric($num2))) {
            return $num1 * $num2;
        }
    } elseif ($operator !== "" && $operator === "/") {
        if (($num1 !== "" && is_numeric($num1)) && ($num2 !== "" && is_numeric($num2))) {
            if ($num2 == 0) {
                echo "Impossible de diviser par 0 !";
            } else {
                return $num1 / $num2;
            }
        }
    }
}
