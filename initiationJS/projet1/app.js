/* Définition des variables */
const buttonNewTask = document.getElementById("newTask");
const resetButton = document.getElementById("reset");
const ol = document.querySelector(".taskList");
const inputTask = document.getElementById("task");

/**
 * Création d'une nouvelle tâche
 * Création de la tâche en question et
 * ajout d'un nouvel li (+ boutons check et delete) dans une liste ordonnée
 * Définition des fonctions pour l'action des boutons check et delete
 * @param {Event} e
 */
const addTask = (e) => {
    e.preventDefault();

    // Création de l'élément li, de son texte et des 2 boutons (check et delete)
    let inputTaskValue = inputTask.value; // récupère valeur du formulaire (input)
    const newLi = document.createElement("li");

    newLi.innerHTML = `<span>${inputTaskValue}</span> <button class="check">&#10003;</button> <button class="delete">X</button>`;

    // Assigne l'élément li avec les infos du input dans le ol
    if (inputTaskValue != "") {
        ol.appendChild(newLi);
    }
    inputTask.value = ""; // on efface le contenu du input

    // On récupère l'ensemble des boutons dans un tableau (NodeList)
    const checkButtons = document.querySelectorAll(".check");
    const deleteButtons = document.querySelectorAll(".delete");

    function checkFunction(e) {
        let checkedTask = e.target.previousElementSibling; // on récupère le frère précédent de la cible
        checkedTask.style.textDecoration = "line-through"; // on barre le texte
    }

    // J'assigne un écouteur d'évènement sur chaque bouton check
    checkButtons.forEach((checkButton) => {
        checkButton.addEventListener("click", checkFunction);
    });

    function deleteFunction(e) {
        e.target.parentElement.remove(); // on supprime l'élément li
    }

    // J'assigne un écouteur d'évènement sur chaque bouton delete
    deleteButtons.forEach((deleteButton) => {
        deleteButton.addEventListener("click", deleteFunction);
    });
};

// J'assigne l'écouteur d'évènement au clic sur le bouton "Nouvelle tâche"
buttonNewTask.addEventListener("click", addTask);

// Reset TODO list
const reset = () => {
    ol.innerHTML = "";
};

resetButton.addEventListener("click", reset);
