<?php
require_once "calculate.php";
?>

<div class="container">
    <h1>Calculatrice simple en PHP</h1>
    <form action="index.php" method="GET">
        <div class="num1-card">
            <label class="number" for="num1">Nombre 1</label>
            <input class="num-input" type="number" name="num1" id="num1" required placeholder="Entrez votre première nombre">
        </div>

        <select name="operator" id="operator" required>
            <option value="+" selected>+</option>
            <option value="-">-</option>
            <option value="*">*</option>
            <option value="/">/</option>
        </select>
        <div class="num2-card">
            <label class="number" for="num2">Nombre 2</label>
            <input class="num-input" type="number" name="num2" id="num2" required placeholder="Entrez votre deuxième nombre">
        </div>

        <button type="submit">Calculer</button>
    </form>
    <p class="result">Résultat : <?php echo round(calculate($num1, $num2, $operator), 3); ?></p>
</div>