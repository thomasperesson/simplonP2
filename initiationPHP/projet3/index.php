<?php
$red = str_pad(dechex($_GET['red']), 2, "0", STR_PAD_LEFT);
$green = str_pad(dechex($_GET['green']), 2, "0", STR_PAD_LEFT);
$blue = str_pad(dechex($_GET['blue']), 2, "0", STR_PAD_LEFT);
$dechex = $red . $green . $blue;
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>

<body>
    <p style="background: #<?= $dechex ?>">
        Ma couleur exadecimal est : #<?= $dechex ?>
    </p>
    <form target="#" method="GET">
        rgb(<input min="0" max="255" step="1" name="red" id="red" type="number">,
        <input min="0" max="255" step="1" name="green" id="green" type="number">,
        <input min="0" max="255" step="1" name="blue" id="blue" type="number">)
        <input type="submit" value="Submit">
    </form>
</body>

</html>